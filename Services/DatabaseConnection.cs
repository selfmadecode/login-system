﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MvcLogin.Services
{
    public class DatabaseConnection
    {
        public SqlConnection sqlCon;

        public DatabaseConnection()
        {
            sqlCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\SelfMade\source\repos\MvcLogin\App_Data\LoginDb.mdf;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework");
        }
        public void OpenConnection()
        {
            if (sqlCon.State != ConnectionState.Open)
            {
                sqlCon.Open();
            }
        }
        public void CloseConnection()
        {
            if (sqlCon.State != ConnectionState.Closed)
            {
                sqlCon.Close();
            }
        }
    }
}