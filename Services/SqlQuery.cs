﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MvcLogin.Models;
using System.Web.Mvc;
using MvcLogin.Controllers;

namespace MvcLogin.Services
{
    public class SqlQuery: Controller
    {
        readonly DatabaseConnection databaseConnection = new DatabaseConnection();
        public void CreateAccount(string query, string userName, string passWord)
        {
            databaseConnection.OpenConnection();
            ////////
            //using (SqlCommand sqlCmd = new SqlCommand(checkDb, databaseConnection.sqlCon))
            //{
            //    throw new InvalidOperationException("UserName already exist");
            //}

            //first check the DB for same username
            // get the details of rows with the same username
            string checkDb = "SELECT COUNT(1) FROM users WHERE UserName=@UserName";
            SqlCommand sqlCheckDbCmd = new SqlCommand(checkDb, databaseConnection.sqlCon);

            sqlCheckDbCmd.Parameters.AddWithValue("@UserName", userName);

            int numberOfUsers = Convert.ToInt32(sqlCheckDbCmd.ExecuteScalar()); // gets the number of rows with the same username

            if (numberOfUsers > 0)
            {
                // user exist
                throw new InvalidOperationException("UserName already exist");
            }
            else
            {
                // userName doesnt exist
                SqlCommand sqlCmd = new SqlCommand(query, databaseConnection.sqlCon);
                sqlCmd.Parameters.AddWithValue("@UserName", userName);
                sqlCmd.Parameters.AddWithValue("@PassWord", passWord);
                sqlCmd.ExecuteNonQuery();
                // var result = sqlCmd.ExecuteNonQuery();
            }
            databaseConnection.CloseConnection();

            //////////

            /*
            // Previous
            SqlCommand sqlCmd = new SqlCommand(query, databaseConnection.sqlCon);

            sqlCmd.Parameters.AddWithValue("@UserName", userName);
            sqlCmd.Parameters.AddWithValue("@PassWord", passWord);
            databaseConnection.CloseConnection();
            var result = sqlCmd.ExecuteNonQuery();
            */

            // inherited the controller class to enable the use of viewbag

        }
        public void LoginUser(string query, string userName, string passWord)
        {

            databaseConnection.OpenConnection();
            SqlCommand sqlCmd = new SqlCommand(query, databaseConnection.sqlCon);

            // Session["UserName"] = userName;
            // the session throws a null reference exception
            sqlCmd.Parameters.AddWithValue("@UserName", userName);
            sqlCmd.Parameters.AddWithValue("@Password", passWord);

            int count = Convert.ToInt32(sqlCmd.ExecuteScalar()); // gets the number of users(rows) with username and password
            databaseConnection.CloseConnection();

            // validate the user input against DB data

            if (count != 1) // if the user not in record, no rows returned
            {
                throw new ArgumentException("Invalid details! Username or Password is incorrect");
            }
        }
    }
}