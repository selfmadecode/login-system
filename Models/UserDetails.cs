﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcLogin.Models
{
    public class UserDetails
    {
     
        [Required(ErrorMessage = "User name is required")] // annotations
        public string UserName { get; set; }

        [Required(ErrorMessage = "User name is required")]
        [DataType(DataType.Password)]
        public string PassWord { get; set; }
    }
}