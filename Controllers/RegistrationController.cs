﻿using MvcLogin.Models;
using MvcLogin.Services;
using System;
using System.Web.Mvc;

namespace MvcLogin.Controllers
{
    public class RegistrationController : Controller
    {
        SqlQuery queryHelper = new SqlQuery(); // queries
        // GET: Registration
        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }
        public ActionResult ProcessRegistration(UserDetails userDetail, string ConfirmPassword)
        {

            if (userDetail.UserName == null || userDetail.PassWord == null)
            {
                ViewBag.validation = false;
                ViewBag.Error = "Invalid input";
                return View("Registration");
            }
            else if (userDetail.PassWord != ConfirmPassword)
            {
                ViewBag.password = false;
                ViewBag.Error = "Password Mismatch!";
                return View("Registration");
            }
            try
            {
                string query = "INSERT INTO users (UserName, PassWord) VALUES (@UserName, @PassWord)";
                // COLLATE SQL_Latin1_General_Cp1_CS_AS keeps the case sensitive
                // LET THE USERNAME BE CASE INSENSITIVE, NO NEED FOR .ToLower()
                // Encrypt password
                queryHelper.CreateAccount(query, userDetail.UserName, userDetail.PassWord);
                ViewBag.AccountCreated = true;
                ViewBag.Info = "Account Created, Login to continue";
                return View("Registration");
            }

            catch (InvalidOperationException ex)
            {
                ViewBag.AccountCreated = true;
                ViewBag.Info = ex.Message;
                return View("Registration");
            }
            catch (Exception ex)
            {
                ViewBag.AccountCreated = true;
                ViewBag.Info = ex.Message;
                return View("Registration");
            }

        }

    }
}