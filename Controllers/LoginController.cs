﻿using MvcLogin.Models;
using MvcLogin.Services;
using System;
using System.Web.Mvc;

namespace MvcLogin.Controllers
{
    public class LoginController : Controller
    {
        SqlQuery sqlQuery = new SqlQuery();
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        // [ValidateAntiForgeryToken]

        public ActionResult Login(UserDetails userDetail)
        {
            if (userDetail.UserName == null || userDetail.PassWord == null)
            {
                ViewBag.validation = false;
                ViewBag.Error = "Invalid input";
                return View("Login");
            }
            // validate epty fields
            try
            {
                string query = "SELECT COUNT(1) FROM users WHERE UserName=@UserName AND PassWord=@Password COLLATE SQL_Latin1_General_Cp1_CS_AS";
                // COLLATE SQL_Latin1_General_Cp1_CS_AS keeps the casing for the password
                // username is case insensitive, if you want it to be case sensitive add COLLATE SQL_Latin1_General_Cp1_CS_AS before AND
                //COLLATE latin1_bin (try this)
                sqlQuery.LoginUser(query, userDetail.UserName, userDetail.PassWord);
                Session["UserName"] = userDetail.UserName.ToLower();
                return View("UserDashBoard");
            }
            catch (ArgumentException ex)
            {
                // Invalid details
                ViewBag.NoUser = true;
                ViewBag.Error = ex.Message;
               return View("Login");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Login");
            }
        }
    }
}